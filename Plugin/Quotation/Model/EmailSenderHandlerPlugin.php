<?php
/**
 * Copyright (c) 2021. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\AutoProposal\Plugin\Quotation\Model;

/**
 * Class EmailSenderHandlerPlugin
 *
 * @package Cart2Quote\AutoProposal\Plugin\Quotation\Model
 */
class EmailSenderHandlerPlugin
{
    /**
     * @var \Cart2Quote\AutoProposal\Model\Quote\AutoProposal\Strategy\StrategyProvider
     */
    protected $strategyProvider;

    /**
     * EmailSenderHandlerPlugin constructor.
     *
     * @param \Cart2Quote\AutoProposal\Model\Quote\AutoProposal\Strategy\StrategyProvider $strategyProvider
     */
    public function __construct(
        \Cart2Quote\AutoProposal\Model\Quote\AutoProposal\Strategy\StrategyProvider $strategyProvider
    ) {
        $this->strategyProvider = $strategyProvider;
    }

    /**
     * @param \Cart2Quote\Quotation\Model\EmailSenderHandler $subject
     * @param array $ignoreStatus
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeSendEmails(
        \Cart2Quote\Quotation\Model\EmailSenderHandler $subject,
        $ignoreStatus = []
    ) {
        //only for the proposal email (we can't plugin on virtual types, so this is how we check that)
        if ($subject->getEmailSender()->getSendEmailIdentifier() == \Cart2Quote\Quotation\Api\Data\QuoteInterface::SEND_PROPOSAL_EMAIL) {
            $delayAmount = $this->strategyProvider->getStrategy()->getDelayAmount();
            if ($delayAmount > 0) {
                $ignoreStatus[] = \Cart2Quote\Quotation\Model\Quote\Status::STATUS_AUTO_PROPOSAL_SENT;
            }
        }

        return [$ignoreStatus];
    }
}
