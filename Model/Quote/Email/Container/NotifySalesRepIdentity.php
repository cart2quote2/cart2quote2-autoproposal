<?php
/**
 * Copyright (c) 2021. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Cart2Quote
 */

namespace Cart2Quote\AutoProposal\Model\Quote\Email\Container;

/**
 * Class NotifySalesRepIdentity
 *
 * @package Cart2Quote\AutoProposal\Model\Quote\Email\Container
 */
class NotifySalesRepIdentity extends \Cart2Quote\Quotation\Model\Quote\Email\Container\AbstractQuoteIdentity
{
    /**
     * Configuration paths
     */
    const XML_PATH_EMAIL_COPY_METHOD = 'quotation_email/notify_salesrep/copy_method';
    const XML_PATH_EMAIL_COPY_TO = 'quotation_email/notify_salesrep/copy_to';
    const XML_PATH_EMAIL_IDENTITY = 'quotation_email/notify_salesrep/identity';
    const XML_PATH_EMAIL_TEMPLATE = 'quotation_email/notify_salesrep/template';
    const XML_PATH_EMAIL_ENABLED = 'quotation_email/notify_salesrep/enabled';

    /**
     * @var \Magento\Framework\Mail\Template\SenderResolverInterface
     */
    protected $senderResolver;

    /**
     * QuoteProposalAcceptedIdentity constructor.
     *
     * @param \Magento\Framework\Mail\Template\SenderResolverInterface $senderResolver
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\Mail\Template\SenderResolverInterface $senderResolver,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($scopeConfig, $storeManager);
        $this->senderResolver = $senderResolver;
    }

    /**
     * @return string
     */
    public function getRecieverEmail()
    {
        $emailIdentity = $this->senderResolver->resolve($this->getEmailIdentity());

        return $emailIdentity['email'];
    }

    /**
     * @return string
     */
    public function getRecieverName()
    {
        $emailIdentity = $this->senderResolver->resolve($this->getEmailIdentity());

        return $emailIdentity['name'];
    }

    /**
     * Get send copy to salesrep setting
     * - This structure seems to be replaced by \Cart2Quote\SalesRep\Plugin\SenderBuilder::beforeSend
     * - This is here for legacy purposes
     *
     * @return bool
     */
    public function isSendCopyToSalesRep()
    {
        return false;
    }
}
