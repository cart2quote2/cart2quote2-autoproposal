<?php
/*
 * Copyright (c) 2021. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cart2Quote\AutoProposal\Setup;

/**
 * Class ReplaceConfigPaths
 *
 * @package Cart2Quote\AutoProposal\Setup
 */
class ReplaceConfigPaths extends \Cart2Quote\AutoProposal\Setup\UpgradeData
{
    /**
     * New Configuration paths to replace pre Cart2Quote 4.1.0 paths
     *
     * @var array
     */
    public $newConfigPaths;

    /**
     * @var \Magento\Quote\Setup\QuoteSetup
     */
    protected $quoteSetup;

    /**
     * ReplaceConfigPaths constructor.
     *
     * @param \Magento\Quote\Setup\QuoteSetup $quoteSetup
     */
    public function __construct(
        \Magento\Quote\Setup\QuoteSetup $quoteSetup
    ) {
        $this->quoteSetup = $quoteSetup;
    }

    /**
     * Replace old config path routes with the new paths
     */
    protected function processConfigPaths()
    {
        $connection = $this->quoteSetup->getConnection();
        $coreConfigTable = $this->quoteSetup->getTable('core_config_data');

        $newConfigPaths["cart2quote_advanced/proposal/auto_proposal"]                                              = "quotation_advanced/proposal/auto_proposal";
        $newConfigPaths["cart2quote_advanced/proposal/auto_proposal_delay"]                                        = "quotation_advanced/proposal/auto_proposal_delay";
        $newConfigPaths["cart2quote_advanced/proposal/auto_proposal_strategy"]                                     = "quotation_advanced/proposal/auto_proposal_strategy";
        $newConfigPaths["cart2quote_advanced/proposal/auto_proposal_ranges"]                                       = "quotation_advanced/proposal/auto_proposal_ranges";
        $newConfigPaths["cart2quote_email/notify_salesrep/enabled"]                                                = "quotation_email/notify_salesrep/enabled";
        $newConfigPaths["cart2quote_email/notify_salesrep/identity"]                                               = "quotation_email/notify_salesrep/identity";
        $newConfigPaths["cart2quote_email/notify_salesrep/template"]                                               = "quotation_email/notify_salesrep/template";
        $newConfigPaths["cart2quote_email/notify_salesrep/copy_to"]                                                = "quotation_email/notify_salesrep/copy_to";
        $newConfigPaths["cart2quote_email/notify_salesrep/copy_method"]                                            = "quotation_email/notify_salesrep/copy_method";

        foreach ($newConfigPaths as $oldPath => $newPath) {
            if(!$connection->query("SELECT * FROM {$coreConfigTable} WHERE 'path' LIKE '{$newPath}'")) {
                $connection->query("UPDATE {$coreConfigTable} SET `path` = REPLACE(`path`, '" . $oldPath . "', '" . $newPath . "') WHERE `path` = '" . $oldPath . "'");
            }
        }
    }
}
