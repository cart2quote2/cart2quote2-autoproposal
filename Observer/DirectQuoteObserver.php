<?php
/**
 * CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2020] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     AutoProposal
 * @copyright   Copyright (c) 2021. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\AutoProposal\Observer;

/**
 * Class CreateQuoteObserver
 *
 * @package Cart2Quote\AutoProposal\Observer
 */
class DirectQuoteObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * Flag to make the observer is executed once
     *
     * @var bool
     */
    protected static $isCalled = false;

    /**
     * @var \Cart2Quote\AutoProposal\Model\Quote\AutoProposal\Strategy\StrategyProvider
     */
    private $strategyProvider;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Cart2Quote\Quotation\Model\QuoteFactory
     */
    private $quoteFactory;

    /**
     * QuoteRequestObserver constructor.
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Cart2Quote\AutoProposal\Model\Quote\AutoProposal\Strategy\StrategyProvider $strategyProvider
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Cart2Quote\Quotation\Model\QuoteFactory $quoteFactory,
        \Cart2Quote\AutoProposal\Model\Quote\AutoProposal\Strategy\StrategyProvider $strategyProvider
    ) {
        $this->strategyProvider = $strategyProvider;
        $this->logger = $logger;
        $this->quoteFactory = $quoteFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        self::$isCalled = true;
        try {
            $quote = $observer->getQuote();
            $this->strategyProvider->getDirectStrategy()->propose($quote);
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}
